//
//  GameScene.swift
//  Pah
//
//  Created by DMYTRO KOMAR on 11/25/16.
//  Copyright © 2016 DMYTRO KOMAR. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var heroNode : SKSpriteNode?
    
    let spaceshipCategory   : UInt32 = 0x1 << 0
    let bulletCategory : UInt32 = 0x1 << 1
    let meteorCategory  : UInt32 = 0x1 << 2
    let PaddleCategory : UInt32 = 0x1 << 3
    let BorderCategory : UInt32 = 0x1 << 4
    var spaceship: SKSpriteNode!
    var bullet: SKSpriteNode!
    var meteor: SKSpriteNode!
    var recorderTimer: Timer!
    var audioURL: URL!
    var isSilent = true
    var isShoted = false
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    override func didMove(to view: SKView) {
        self.anchorPoint = CGPoint(x: 0, y: 0)
        physicsWorld.contactDelegate = self
        
        
        self.spaceship = childNode(withName: "Spaceship") as! SKSpriteNode
        self.spaceship.position = CGPoint(x: self.frame.minX + self.spaceship.frame.width / 2, y: self.frame.midY)
        self.recordTapped()
        
        self.spawnMeteor()
        
    }
    
    func spawnMeteors() {
        for _ in 0...4 {
            self.spawnMeteor()
        }
    }
    
    func spawnMeteor() {
        
        self.meteor = SKSpriteNode(imageNamed: "meteor")
        let randomMeterSize = Int((arc4random_uniform(100)))
        self.meteor.size = CGSize(width: randomMeterSize, height: randomMeterSize)
        self.meteor.physicsBody = SKPhysicsBody(circleOfRadius: self.meteor.size.width / 2)
        
        
        
        
        self.meteor.position = CGPoint(x: self.frame.maxX, y: CGFloat(arc4random_uniform(UInt32(self.frame.maxY))))
        
        self.meteor.run(SKAction.moveTo(x: 0, duration: 6)) {
            self.meteor.removeFromParent()
            self.spawnMeteor()
        }
        self.meteor.physicsBody?.affectedByGravity = false
        self.meteor.physicsBody?.categoryBitMask = meteorCategory
        self.meteor.physicsBody?.contactTestBitMask = bulletCategory
        
        self.addChild(self.meteor)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !self.isShoted {
            self.isShoted = true
            self.bullet = SKSpriteNode(imageNamed: "ball")
            self.bullet.size = CGSize(width: 20, height: 20)
            self.bullet.physicsBody = SKPhysicsBody(circleOfRadius: self.bullet.size.width / 2)
            self.bullet.physicsBody?.affectedByGravity = false
            self.addChild(self.bullet)
            self.bullet.position = CGPoint(x: self.spaceship.frame.maxX, y: self.spaceship.frame.midY)
            self.bullet.run(SKAction.moveTo(x: self.frame.size.width, duration: 3)) {
                self.bullet.removeFromParent()
                self.isShoted = false
            }
            self.bullet.physicsBody?.categoryBitMask = bulletCategory
            self.bullet.physicsBody?.collisionBitMask = bulletCategory | meteorCategory
            self.bullet.physicsBody?.contactTestBitMask = bulletCategory | meteorCategory
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch contactMask {
        case bulletCategory | meteorCategory:
            self.bullet.removeFromParent()
            self.meteor.removeFromParent()
            self.isShoted = false
            self.spawnMeteor()
        default: break
        }

    }
    
    func startRecording() {
        
        let dirPath =  NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let recordName = "record.wav"
        
        //need to test
        self.audioURL = URL(fileURLWithPath: dirPath + "/" + recordName)
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, with:.defaultToSpeaker)
        
        do {
            self.audioRecorder = try AVAudioRecorder(url: self.audioURL, settings: [:])
            self.audioRecorder.delegate = self
            self.audioRecorder.prepareToRecord()
            self.audioRecorder.record()
            self.audioRecorder.isMeteringEnabled = true
            
        } catch {
            finishRecording(success: false)
        }
    }
    
    var currentRes = 0.0
    func levelTimerCallback() {
        self.audioRecorder.updateMeters()
        let peakPowerForChannel = pow(3, (0.07 * self.audioRecorder.peakPower(forChannel: 0)))
        let lowPassResults =  Double(peakPowerForChannel) * Double(Int(self.frame.size.height))
        print(lowPassResults)
        
        if lowPassResults > 100.0 {
            self.moveUp()
            self.currentRes = lowPassResults
            return
        }
        if lowPassResults < self.currentRes  {
            self.moveDown()
            return
        }
    }
    
    func moveUp() {
        if self.spaceship.position.y < 320 - self.spaceship.size.width / 2 {
            self.spaceship.run(SKAction.moveTo(y: self.spaceship.position.y + 10, duration: 0.1))
        }
    }
    
    func moveDown() {
        if self.spaceship.position.y > 0 + self.spaceship.size.width / 2 {
            self.spaceship.run(SKAction.moveTo(y: self.spaceship.position.y - 10, duration: 0.1))
        }
        
    }
    
    func recordTapped() {
        if audioRecorder == nil {
            self.recorderTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.levelTimerCallback), userInfo: nil, repeats: true)
            self.startRecording()

        } else {
            finishRecording(success: true)
        }
    }
}

extension GameScene: AVAudioRecorderDelegate {
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        
        if success {
            
            print("recording succ :(")
            
        } else {
            // recording failed :(
            print("recording failed :(")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

